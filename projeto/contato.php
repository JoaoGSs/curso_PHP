<?php

use PHPMailer\PHPMailer\PHPMailer;

require_once('php/config.php');
$msg = array();

try 
{
    if ($_POST) 
    {
        $nome = trim($_POST['nome']);
        $email = trim($_POST['email']);
        $mensagem = trim($_POST['mensagem']);

        if (empty($nome)) 
        {
            throw new Exception('Por Favor preencha o campo nome!');
        }

        $email_valido = filter_var($email, FILTER_SANITIZE_EMAIL);
        
        if ($email_valido === false) 
        {
            throw new Exception('Email inválido!');
        }

        if (empty($mensagem))
        {
            throw new Exception('Por Favor preencha o campo mensagem!');
        }

        $to = "joao.seradarian@gmail.com";
        $assunto = "Novo Contato - Site Minha Loja";
        $msg_email = "";

        $msg_email = "Segue, novo contato via site com as seguintes informações:
            NOME: $nome
            E_MAIL: $email
            MENSAGEM:
            $mensagem
        ";

        //$email_enviado = mail($to, $assunto, $msg_email);
        $mail = new PHPMailer();
        $mail->isSMTP();
        $mail->Host = 'smtp.umpler.com';
        $mail->Port = 587;
        $mail->SMTPSecure = '';
        $mail->SMTPAutoTLS = false;
        $mail->SMTPAuth = true;        
        $mail->Sender = 'php8410@jhonatanjacinto.dev';
        $mail->Username = 'php8410@jhonatanjacinto.dev';
        $mail->Password = 'zxcv4321';
        $mail->setFrom('contato@minhloja.com.br');
        $mail->addReplyTo($email, $nome);
        $mail->addAddress($to, 'João Gabriel');
        $mail->Subject = $assunto;
        $mail->Body = $msg_email;

        if ($email_enviado)
        {
            $msg = array(
                'estilo' => 'alert alert-success',
                'mensagem' => 'Mensagem enviada com Sucesso!'
            );
        }

        else 
        {
            throw new Exception('Não foi possível enviar sua mensagem');
        }
    }
} 
catch (Exception $e) 
{
    $msg = array(
        'estilo' => 'alert alert-danger',
        'mensagem' => $e->getMessage()
    );
}

$titulo_pagina = 'Contato';
require_once('php/includes/cabecalho.php');
?>

<h1>Contato</h1>

<?php include_once('php/includes/mensagem.php'); ?>

<form action="" method="POST">
    <div class="form-group">
        <label>Nome:</label>
        <input class="form-control" type="text" name="nome" />
    </div>
    <div class="form-group">
        <label>E-mail:</label>
        <input class="form-control" type="text" name="email" />
    </div>
    <div class="form-group">
        <label>Mensagem:</label>
        <textarea name="mensagem" class="form-control"></textarea>
    </div>
    <button type="submit" class="btn btn-primary">Enviar</button>
</form>

<?php require_once('php/includes/rodape.php'); ?>