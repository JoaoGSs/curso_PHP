<meta charset="utf8">
<h1>Estrutura de Repetição (foreach)</h1>

<?php



$estados = array(
    'AC' => 'Acre',
    'AM' => 'Amazonas',
    'RR' => 'Roraima',
    'RO' => 'Rondônia',
    'PA' => 'Pará',
    'TO' => 'Tocantins',
    'AP' => 'Amapá'
);

foreach ($estados as $sigla => $nome_estado) {
    echo "A Sigla é: $sigla e o estado é $nome_estado <br>";
}
echo '<br>'
?>

<strong>Região Norte:</strong>
<select>
    <option value="">--Selecione um Estado--</option>
    <?php foreach ($estados as $sigla => $estado) : ?>
        <option value="<?= $sigla ?>">
            <?= $estado ?>
        </option>
    <?php endforeach; ?>
</select>
<br><br>

<?php
//array multidimensional

$usuarios = array (
    (object)array( #usuário 1
        'nome_usuario' => 'Jhonatan Jacinto',
        'email' => 'jhonatan.jacinto@caelum.com.br',
        'senha' => 'abcd1234'
    ),
    (object)array( #usuário 2
        'nome_usuario' => 'Adriana Favero',
        'email' => 'adriana.favero@caelum.com.br',
        'senha' => 'qwr123'
    )
);

/*foreach ($usuarios as $usuario) {            usando array multidimensional
    echo $usuario['nome_usuario'] . '<br>';*/

    foreach ($usuarios as $usuario) {
        echo $usuario->nome_usuario . '<br>';    //typecast
}

#typeCast - só funciona com chave nomeada

