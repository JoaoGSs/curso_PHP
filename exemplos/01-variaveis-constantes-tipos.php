<meta charset="utf-8">
<h1> Variáveis, Constantes e Tipos</h1>
<?php
// ou # comentário de uma linha 
/*comentario de bloco*/

# Toda a variável começa com $

$texto = "Texto (string) com aspas duplas!";
$texto2 = 'Texto (string) com aspas simples';

$boleana = true;
$decimal = 10.57;
$inteiro = 10;

/*
String - texto
Boolean - false/true
Float - decimal
Integer - inteiro
Array - lista de dados
Object - objeto (OOP)
Resource - rescurso externo/ponteiro
Null - nulo
*/

$lista_simples = ['Arroz',"Feijão","Batata",12, true, 15.50];
$lista_simples2 = ['Arroz',"Óleo",12, true, 15.50];
$lista_chave_nomeada = array(
'nome_completo' => 'Jhonatan Jacinto',
'idade' =>  28,
'email' => 'jhonatan.jacinto@caelum.com.br',
'esta_empregado' => true
);

print $lista_simples[1];
print '<br>';
print 'Nome do Empregado' . $lista_chave_nomeada['nome_completo']; 
print '<br>';

# exibição de Informações usando CONCATENAÇÃO
$nome_produto = 'Micro-ondas';
$preco_produto = 12578.586;

print "O produto " . $nome_produto . " tem o custo R$" .$preco_produto;
print '<br>';

# exibição de Informações usando interpolação
# só funciona com aspas duplas 
# só funciona com variável
print "O produto $nome_produto tem o custo R$ $preco_produto";
print '<br>';
print "O e-mail do empregado é {$lista_chave_nomeada['email']}";

# Constantes - sempre maiúslcula
define('MINHA_IDADE', 28);
const MEU_EMAIL = 'jhonatan.jacinto@caelum.com.br';
print '<br>';
print "Minha idade é " . MINHA_IDADE . " anos";
print '<br>meu email é: ' .MEU_EMAIL;
print '<br>';

# Exibição Formatada (printf) 
# Exibir as informações numa estrutura de template onde as áreas reservadas serão substituídas pelos valores passados
printf("O produto %s, tem o custo de R$ %.2f", $nome_produto, $preco_produto);
echo '<br>';

# formatação de números
$preco_produto = 1578859.658;
echo "R$ " . number_format($preco_produto, 2, ',', '.');