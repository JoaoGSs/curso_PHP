<?php

include('../projeto/php/functions/utils.php');

class Cliente {
    public $id = 0;
    public $nome = '';
    public $email = '';
    public $desconto = 10;
    protected $estilo = 'alert';
    public function __construct()
    {
        print $this->estilo;
    }

    public function apresenteSe()
    {
        return 'Eu sou um cliente';
    }
}

#Conceito de herança, onde a classe ClienteVip herda todos os atributos da classe Cliente e sobre escreve os dados que ela tem (desconto não vai ser 10 mas sim 60) e adiciona o dados próprios da sua classe
class ClienteVip extends Cliente
{ 
    public $desconto = 60;
    public $temVantagens = true;
    public function __construct()
    {
        print $this->estilo;
    }

    public function apresenteSe()
    {
        print 'Eu sou um cliente VIP';
    }
}

$cliente = new Cliente();
$cliente->apresenteSe();

