<meta charset="utf-8">
<h1>MySQLi Procedural</h1>

<?php

#mysqli_CONECT_errno -> usado para problemas de conexão antes de conectar com o banco
$conexao = mysqli_connect('localhost','root','admin','loja');
if(mysqli_connect_errno()){
    echo 'Não foi possível conectar no banco de dados' . mysqli_connect_errno();
    exit();
}
mysqli_set_charset($conexao,'utf8'); #faz com que a conexção 'conversa entre o html o php e o banco esteja no padrão utf8'

echo 'Conexão efetuada com sucesso!' . '<br>';

#mysqli_ERRNO -> usado para problemas após ter conectado no banco
$resultado = mysqli_query($conexao, "SELECT * FROM categorias");
if(mysqli_errno($conexao)){
    echo 'Erro ao realizar operação no banco de dados ' . mysqli_error($conexao);
    exit();
}

/*while($registro = mysqli_fetch_assoc($resultado)){
    echo $registro['nome'] . '<br>';
}*/

$lista_categorias = mysqli_fetch_all($resultado, MYSQLI_ASSOC);
foreach($lista_categorias as $categoria) {
    echo $categoria['nome'] . '<br>';
}

