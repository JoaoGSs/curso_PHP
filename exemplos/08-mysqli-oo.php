<meta charset="utf-8">
<h1>MySQLi Orientado a Objeto</h1>

<?php

$conexao = new mysqli('localhost', 'root', 'admin', 'loja');
if ($conexao->connetc_errno) 
{
    die('Não foi possível conectar com o banco de dados' . $conexao->connect_errno);
}
$conexao->set_charset(utf8);

# Query
$resultado = $conexao->query('SELECT * FROM categorias');
if ($conexao->errno) 
{
    die('Erro ao realizar operação no banco de dados!' . $conexao->errno);
}

# fetch_assoc(): retorna um registro por vez
while ($registro = $resultado->fetch_assoc())
{
    echo $registro['nome'] . '<br>';
}

print'<br><br>';

# fetch_all(): retorna todos os registros de uma vez
$listaRegistro = $resultado->fetch_all(MYSQLI_ASSOC);
foreach ($listaRegistros as $categoria) 
{
    echo $categoria['nome'] . '<br>';
}