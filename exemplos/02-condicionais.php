<meta charset="utf-8">
<h1>Estruturas Condicionais</h1>

<?php
$idade = 13;

if ($idade >= 12 && $idade <=17) {
    echo 'Plano Teen';
}

else if ($idade >= 18 && $idade <=29){
    echo 'Plano Professional';
}

else if ($idade >= 30){
    echo 'Plano Xperience';
}
else {
    echo 'Não há planos para você';
}

echo '<br><br>';

# Sintaxe herdada do Python/Ruby/Pearl
# normalmente usada quando há SQL no meio

$idade = 18;

if ($idade >= 12 && $idade <= 17) :
    echo 'Plano Teen';

elseif ($idade >= 18 && $idade <= 29) :
    echo 'Plano Profissional';

elseif ($idade >= 18 && $idade <= 29) :
    echo 'Plano Xperience';

   else :
    echo 'Não há planos para você!';
endif;

echo '<br><br>';

# Switch / Case

switch ($idade) {
    case 18 :
        echo 'Você acaba de se tornar um adulto!';
    break;
    case 30 :
    case 31 :
        echo 'Você se tornou um audlto maduro!';
    break; 

    default:
    echo 'Não sei analisar sua idade';
    break;
}
echo '<br><br>';

#Operador Ternário ( () ? : ; )
$idade = 21;
$mensagem = ($idade < 18) ? 'Você não tem permissão para acessar!' : 'Você tem Permissão!';
echo $mensagem;

