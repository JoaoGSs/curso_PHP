<meta charset="utf8">
<h1>Operações Matemáticas</h1>

<form method="post">
    <label>Número 1: </label>
    <input type="number" name="numero1">
    <br><br>

    <label>Número 2: </label>
    <input type="number" name="numero2">
    <br><br>

    <button name="botao" value="adicao">+</button>
    <button name="botao" value="subtrair">-</button>
    <button name="botao" value="multiplicar">X</button>
    <button name="botao" value="dividir">/</button>
</form>    

<?php

#1 Verifica se num1 e num2 são dados numéricos e estão definidos
if (isset($_POST['numero1'], $_POST['numero2']) and is_numeric($_POST['numero1']) and is_numeric($_POST['numero2'])) {
    
    #2 Guardar o valor do GET nas variáveis
    $numero1 = $_POST['numero1'];
    $numero2 = $_POST['numero2'];
    
    #3 checar qual botão foi clicado
    if ($_POST['botao'] == 'adicao') {
        echo 'o Rsultado é: ' . ($numero1 + $numero2);
    }

    else if ($_POST['botao'] == 'subtrair') {
    echo 'o Rsultado é: ' . ($numero1 - $numero2);
    }    

    else if ($_POST['botao'] == 'multiplicar') {
    echo 'o Rsultado é: ' . ($numero1 * $numero2);
    }

    else if ($_POST['botao'] == 'dividir') {
    echo 'o Rsultado é: ' . ($numero1 / $numero2);
    }    
    
}
