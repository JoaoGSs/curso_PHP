<meta charset="utf8">
<h1>Valores Considerados Falsos e Verdadeiros</h1>

<?php

/* Valores considerados FALSOS */
$var1 = (bool)false;
$var2 = (bool)'';
$var3 = (bool)array();
$var4 = (bool)0;
$var5 = (bool)'0';
$var6 = (bool)null;
$var7 = (bool)0.0;

var_dump($var1,$var2,$var3,$var4,$var5,$var6,$var7);

echo'<br><br>';

/* Valores considerados VERDADEIROS */
$var8 = (bool)'false';
$var9 = (bool)'0.0';
$var10 = (bool)array('teste1');
$var11 = (bool)75;
$var12 = (bool)new stdClass();

var_dump($var8,$var9,$var10,$var11,$var12);