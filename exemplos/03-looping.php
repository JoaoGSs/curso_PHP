<meta charset="utf-8">
<h1>Estrutura de Repetição (Tabuada)</h1> 

<form action="">
    <label>Tabuada do Nº:</label>
    <input type="number" name="numero" id="numeroEscolhido">
    <button>Ver Tabuada</button>
</form>

<?php
# GET variável global que tem todas as infiormações 
#isset(): verifica se uma informação Existe
#is_numeric(): verufica se a infirmação é numérica 

if(isset($_GET['numero']) and is_numeric($_GET['numero'])){

    echo 'O número é: ' . $_GET['numero'] . '<br>';
    $contador = 1;
    $numero = $_GET['numero'];

    /*while ($contador <= 10) {
        $resultado = $contador * $numero;
        echo "$numero x $contador = $resultado <br>";
        $contador = $contador + 1; # ou $contador++;
    }*/

    for ($contador = 1; $contador <= 10; $contador++){
        $resultado = $contador * $numero;
        echo "$numero x $contador = $resultado <br>";
    }
    
}

